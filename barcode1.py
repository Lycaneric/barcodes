# Import required packages
import argparse
import cv2

from pyzbar import pyzbar

# Construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument('-i', '--image', required=True, help='path to image')
args = vars(ap.parse_args())

# Load the input image
image = cv2.imread(args['image'])

# Find the barcodes in the image and decode each of them
barcodes = pyzbar.decode(image)

# Loop over the detected barcodes
for barcode in barcodes:
    # Extract the bounding box location and draw it
    (x, y, w, h) = barcode.rect
    cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)

    # Convert barcode data to string from bytes
    barcodeData = barcode.data.decode('utf-8')
    barcodeType = barcode.type

    # Draw barcode data and type to image
    text = '{} ({})'.format(barcodeData, barcodeType)
    cv2.putText(image, text, (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)

    # Output to terminal
    print('[INFO] Found {} barcode: {}'.format(barcodeType, barcodeData))

# Show the output image
cv2.imshow('Image', image)
cv2.waitKey(0)

