# Import the required packages
import argparse
import cv2
import datetime
import imutils
import time

from imutils.video import VideoStream

from pyzbar import pyzbar

# Construct and parse argument
ap = argparse.ArgumentParser()
ap.add_argument('-o', '--output', type=str, default='barcodes.csv', help='path to output csv')
args = vars(ap.parse_args())

# Initialize the video stream and allow the camera sensor to warm up
print('[INFO] starting video stream...')
vs = VideoStream(src=0).start()
# vs = VideoStream(usePiCamera=True).start()
time.sleep(2)

#open the output csv file and init the set of barcodes found
csv = open(args['output'], 'w+')
found = set()

# Loop over the frames from the video stream
while True:
    # Grab the fram from the threaded video stream and resize it
    frame = vs.read()
    frame = imutils.resize(frame, width=400)

    # Find the barcodes in the fram and decode each of them
    barcodes = pyzbar.decode(frame)

    # Loop over the detected barcodes
    for barcode in barcodes:
        # Extract the bounding box location of the barcode and draw it
        (x, y, w, h) = barcode.rect
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 0, 255), 2)

        # Convert barcode data from bytes to str
        barcodeData = barcode.data.decode('utf-8')
        barcodeType = barcode.type

        # draw the barcode data and type to the image
        text = '{} ({})'.format(barcodeData, barcodeType)
        cv2.putText(frame, text, (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)

        # If barcode is not in current list, add to list
        if barcodeData not in found:
            csv.write('{}, {}\n'.format(datetime.datetime.now(), barcodeData))
            csv.flush()
            found.add(barcodeData)

    # Show the output frame
    cv2.imshow('Barcode Scanner', frame)
    key = cv2.waitKey(1) & 0xFF

    # Quit if q pressed
    if key == ord('q'):
        break

# Close the output CSV file
print('[INFO] cleaning up...')
csv.close()
cv2.destroyAllWindows()
vs.stop()

